/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puchong.oxandclass;

import java.util.Scanner;

/**
 *
 * @author User
 */
public class Game {

    Scanner kb = new Scanner(System.in);
    Player playerX;
    Player playerO;
    Player turn;
    Table table;
    int countRound = 9;
    int row, col;

    public Game() {
        playerX = new Player('X');
        playerO = new Player('O');
        table = new Table(playerX, playerO);
    }

    public void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    public void showTable() {
        table.showTable();
    }

    public void input() {
        for (;;) {
            System.out.println("Please input Row Col : ");
            row = kb.nextInt() - 1;
            col = kb.nextInt() - 1;

            if (table.setRowCol(row, col)) {
                countRound--;
                break;
            }
            System.out.println("Table at row and col is not empty");
        }
    }

    public void showTurn() {
        System.out.println(table.getCurrenPlayer().getName() + " turn");
    }

    public void run() {
        this.showWelcome();
        while (true) {
            this.showTable();
            this.showTurn();
            this.input();
            table.checkWin();
            if (table.isFinish()) {
                if (table.getWin() == null) {
                    System.out.println("Draw!!");
                } else {
                    System.out.println(table.getWin().toString());
                }
                break;
            }
            table.switchPlayer();
        }
    }

}
