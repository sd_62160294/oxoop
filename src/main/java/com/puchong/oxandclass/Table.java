/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puchong.oxandclass;

/**
 *
 * @author User
 */
public class Table {

    private char[][] table = {{'-','-','-'}, {'-','-','-'}, {'-','-','-'}};
    private Player playerX;
    private Player playerO;
    private Player currenPlayer;
    private Player win;
    private boolean finish = false;
    private int row, col;
    private int countRound = 9;

    public Table(Player x, Player o) {
        playerX = x;
        playerO = o;
        currenPlayer = x;
    }

    public void showTable() {
        System.out.println("  1 2 3");
        for (int i = 0; i < table.length; i++) {
            System.out.print(i + 1 + " ");
            for (int j = 0; j < table.length; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.println();
        }
    }

    public boolean setRowCol(int row, int col) {
        if (table[row][col] == '-') {
            table[row][col] = currenPlayer.getName();
            this.row = row;
            this.col = col;
            return true;
        }
        return false;
    }

    public Player getCurrenPlayer() {
        return currenPlayer;
    }

    public void switchPlayer() {
        if (currenPlayer == playerX) {
            currenPlayer = playerO;
        } else {
            currenPlayer = playerX;
        }
    }

    public void checkWinLoseDraw() {
        if (currenPlayer == playerX) {
            playerX.win();
            playerO.lose();
        } else {
            playerO.win();
            playerX.lose();
        }
    }

    void checkRow() {
        for (int col = 0; col < 3; col++) {
            if (table[row][col] != currenPlayer.getName()) {
                return;
            }
        }
        finish = true;
        win = currenPlayer;
        checkWinLoseDraw();
    }

    void checkCol() {
        for (int row = 0; row < 3; row++) {
            if (table[row][col] != currenPlayer.getName()) {
                return;
            }
        }
        finish = true;
        win = currenPlayer;
        checkWinLoseDraw();
    }

    void checkX() {
        for (int col = 0; col < 3; col++) {
            if (table[col][col] != currenPlayer.getName()) {
                return;
            }
        }
        finish = true;
        win = currenPlayer;
        checkWinLoseDraw();
    }

    void checkY() {
        for (int row = 0, count = 2; row < 3; row++, count--) {
            if (table[row][count] != currenPlayer.getName()) {
                return;
            }
        }
        finish = true;
        win = currenPlayer;
        checkWinLoseDraw();
    }

    public void checkRound() {
        countRound--;
        if (countRound == 0) {
            finish = true;
            win = null;
            checkWinLoseDraw();
        }
    }

    public void checkWin() {
        checkRow();
        checkCol();
        checkX();
        checkY();
        checkRound();
    }

    public boolean isFinish() {
        return finish;
    }

    public Player getWin() {
        return win;
    }

}
